#echo "Arg 1=" $1
#echo "Arg 2=" $2
#echo "Arg 3=" $3
#echo "Arg 4=" $4

if test -z $1; then
	echo "Please provide a filename"
fi

if test ! -e $1; then
	echo 'File does not exist!'
fi
